import React from 'react';
import NavigationBar from "./components/navigationBar/navigationBar";
import { Route, Router, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import routes from "./constants/routes";
import './App.scss';

const history = createBrowserHistory();

function App() {
  return (
      <Router history={history}>
        <div className="app">
          <NavigationBar/>
          <Switch>
            {routes.map((route, index) => (
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.component}/>
            ))}
          </Switch>
        </div>
      </Router>
  );
}

export default App;