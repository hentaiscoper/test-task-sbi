import { getCommentsSuccess, catchCommentsError, requestComments, } from "../actions/postActions";
import { catchUsersError, getUsersSuccess, requestUsers } from "../actions/userActions";
import { catchPostsError, getPostsSuccess } from "../actions/commentsActions";

const URL = "https://jsonplaceholder.typicode.com";


// User referenced api

export function getUsers(id) {
    return dispatch => {
        dispatch(requestUsers());
        return fetchUsers(id).then(([response, json]) => {
            if(response.status === 200) {
                dispatch(getUsersSuccess(json));
            }
            else {
                dispatch(catchUsersError(response.status));}
        })
    }
}

function fetchUsers(id) {
    return fetch(`${URL}/users/${id === undefined ? '' : id}`, {method: "GET"})
        .then( response => Promise.all([response, response.json()]));
}

// Posts referenced api

export function getPosts(id) {
    return dispatch => {
        return fetchPosts(id).then(([response, json]) => {
            if(response.status === 200) {
                dispatch(getPostsSuccess(json));
            }
            else {
                dispatch(catchPostsError(response.status));}
        })
    }
}

function fetchPosts(id) {
    return fetch(`${URL}/posts${id === undefined ? '' : `?userId=${id}`}`, {method: "GET"})
        .then( response => Promise.all([response, response.json()]));
}

// Comments referenced api

export function getComments(id) {
    return dispatch => {
        dispatch(requestComments());
        return fetchComments(id).then(([response, json]) => {
            if(response.status === 200) {
                dispatch(getCommentsSuccess({json, id}));
            }
            else {
                dispatch(catchCommentsError(response.status));}
        })
    }
}

function fetchComments(id) {
    return fetch(`${URL}/comments${id === undefined ? '' : `?postId=${id}`}`, {method: "GET"})
        .then( response => Promise.all([response, response.json()]));
}