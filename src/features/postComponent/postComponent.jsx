import React, { Component } from 'react';
import PropTypes from "prop-types";
import CommentComponent from "../../components/commentComponent/commentComponent";
import { connect } from "react-redux";
import icons from "../../constants/svgs"
import './postComponent.scss';

class PostComponent extends Component{
    get isOpen() {
        const { currentPostId, post: { id } } = this.props;
        return currentPostId === id;

    }

    renderButton = () => {
        return (
            <button
                className={`post-container__button ${this.isOpen ? "post-container__button--collapsed" : 
                    "post-container__comments-section--expanded"}`}
                onClick={() => (this.isOpen ? this.props.collapseComments() :
                    this.props.expandComments(this.props.post.id))}
            >
                <img
                    src={icons.expandArrow}
                    alt="expand"
                />
            </button>
        )
    };

    renderComments = () => {
        const { comments, commentsPending } = this.props;
        return <>
            {
                (this.isOpen && !commentsPending && comments.map((comment, index) =>
                <CommentComponent
                    key={index}
                    comment={comment}
                />))
            }
        </>
    };

    render() {
        const { post } = this.props;
        return (
            <div className="post-container">
                <div className="post-container__title">{post.title}</div>
                <div className="post-container__body">{post.body}</div>
                <div className={`post-container__comments-section ${this.props.currentPostId === this.props.post.id ? 
                    "post-container__comments-section--expanded" : "post-container__comments-section--collapsed"}`}
                >
                    {this.renderComments()}
                </div>
                {this.renderButton()}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.commentsReducer.comments,
    commentsPending: state.commentsReducer.commentsPending,
});

PostComponent.propTypes = {
    post: PropTypes.shape().isRequired,
    comments: PropTypes.array.isRequired,
    commentsPending: PropTypes.bool,
    currentPostId: PropTypes.number.isRequired,
    expandComments: PropTypes.func.isRequired,
    collapseComments: PropTypes.func.isRequired,
};

export default connect(mapStateToProps) (PostComponent);
