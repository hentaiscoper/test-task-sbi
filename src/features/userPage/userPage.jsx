import React, { Component } from 'react';
import PropTypes from "prop-types";
import Feed from "../feed/feed";
import { connect } from "react-redux";
import { getUsers } from "../../api/api";
import UserCard from "../../components/userCard/userCard";


class UserPage extends Component {
    componentDidMount() {
        const { userId } = this.props.match.params;
        this.props.getUsers(userId);
    }

    render() {
        const { props } = this;
        return (
            <div className="container">
                <UserCard userId={this.props.match.params} />
                <Feed {...props}/>
            </div>
        );
    }
}

const mapStateToProps = () => ({
});

UserPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape(),
    }),
    getUsers: PropTypes.func.isRequired,
};

export default connect (mapStateToProps, { getUsers }) (UserPage);