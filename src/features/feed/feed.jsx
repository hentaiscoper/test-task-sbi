import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getComments, getPosts } from "../../api/api";
import PostComponent from "../postComponent/postComponent";
import * as queryString from "query-string";

class Feed extends Component {
    componentDidMount() {
        const { userId } = this.props.match.params;
        this.props.getPosts(userId);
        if (queryString.parse(this.props.location.search).postId !== undefined)
            this.props.getComments(queryString.parse(this.props.location.search).postId);
    }

    get currentPostId () {
        return +queryString.parse(this.props.location.search).postId
    }

    expandComments = postId => {
        this.props.getComments(postId);
        this.props.history.push(`?postId=${postId}`);
    };

    collapseComments = () => {
        this.props.history.push(this.props.location.pathname);
    };

    render() {
        const { posts } = this.props;
        return (
            <div className="container__content">
                {posts !== undefined && posts.map((post, index )=>
                        <PostComponent
                            currentPostId={this.currentPostId}
                            collapseComments={this.collapseComments}
                            expandComments={this.expandComments}
                            key={index}
                            post={post}/>
                            )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.postsReducer.posts,
});

Feed.propTypes = {
    posts: PropTypes.array.isRequired,
    history: PropTypes.shape().isRequired,
    location: PropTypes.shape().isRequired,
    getPosts: PropTypes.func.isRequired,
    getComments: PropTypes.func.isRequired,
    match: PropTypes.shape({
        params: PropTypes.shape()
    })
};

export default connect(mapStateToProps, { getPosts, getComments }) (Feed);