import React, {Component} from 'react';
import PropTypes from "prop-types";
import { connect } from "react-redux";

import UserComponent from "../../components/userComponent/userComponent";
import { getUsers } from "../../api/api";

class UserList extends Component {
    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const { allUsers, usersPending } = this.props;
        return (
            <div className="container">
                <div className="container__title">Users</div>
                <div className="container__content">
                    {!usersPending && allUsers.map((user, index) => <UserComponent key={index} user={user}/>)}
                 </div>
            </div>
        );
    }
}

//

const mapStateToProps = state => ({
    allUsers: state.usersReducer.users,
    usersPending: state.usersReducer.usersPending
});

UserList.propTypes = {
    allUsers: PropTypes.array.isRequired,
    usersPending: PropTypes.bool,
    getUsers: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { getUsers }) (UserList);