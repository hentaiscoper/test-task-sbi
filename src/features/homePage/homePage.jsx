import React, {Component} from 'react';
import Feed from "../feed/feed";

class HomePage extends Component {
    render() {
        const { props } = this;
        return (
            <div className="container">
                <div className="container__title">Home</div>
                <Feed {...props}/>
            </div>
        );
    }
}

export default HomePage;