import place from '../assets/icons/place.svg';
import link from '../assets/icons/link.svg';
import expandArrow from '../assets/icons/expand_arrow.svg';
import home from '../assets/icons/home.svg';
import users from '../assets/icons/users.svg';

const icons = {
    place,
    link,
    expandArrow,
    home,
    users,
};

export default icons;