import UserList from "../features/userList/userList";
import UserPage from "../features/userPage/userPage";
import HomePage from "../features/homePage/homePage";

const routes = [
    {
        path: "/",
        exact: true,
        component: HomePage,
    },
    {
        path: "/home",
        component: HomePage,

    },
    {
        path: "/users",
        component: UserList,
    },
    {
        path: "/user/:userId",
        component: UserPage
    },
];

export default routes;