export const REQUEST_COMMENTS = "REQUEST_COMMENTS";
export const GET_COMMENTS_SUCCESS = "GET_COMMENTS_SUCCESS";
export const CATCH_COMMENTS_ERROR = "CATCH_COMMENTS_ERROR";

export const requestComments = () => {
    return {
        type: REQUEST_COMMENTS,
    }
};

export const getCommentsSuccess = (payload) => {
    return {
        type: GET_COMMENTS_SUCCESS,
        payload,
    }
};

export const catchCommentsError = (payload) => {
    return {
        type: CATCH_COMMENTS_ERROR,
        payload
    }
};
