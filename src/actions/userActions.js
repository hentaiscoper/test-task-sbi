export const REQUEST_USERS = "REQUEST_USERS";
export const GET_USERS_SUCCESS = "GET_USERS_SUCCESS";
export const CATCH_USERS_ERROR = "CATCH_USERS_ERROR";

export const requestUsers = () => {
    return {
        type: REQUEST_USERS,
    }
};

export const getUsersSuccess = (payload) => {
    return {
        type: GET_USERS_SUCCESS,
        payload
    }
};

export const catchUsersError = (payload) => {
    return {
        type: CATCH_USERS_ERROR,
        payload
    }
};