export const GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS";
export const CATCH_POSTS_ERROR = "CATCH_POSTS_ERROR";

export const getPostsSuccess = (payload) => {
    return {
        type: GET_POSTS_SUCCESS,
        payload,
    }
};

export const catchPostsError = (payload) => {
    return {
        type: CATCH_POSTS_ERROR,
        payload
    }
};