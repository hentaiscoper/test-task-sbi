import React from 'react';
import { NavLink } from "react-router-dom";
import icons from "../../constants/svgs";
import './navigationBar.scss';
import ReactSVG from "react-svg";

const NavigationBar = () => {
    return (
            <ul className="nav-list">
                <li className="nav-list__item">
                    <NavLink activeClassName="active" to="/home"><ReactSVG src={icons.home}/><span>Home</span></NavLink>
                </li>
                <li className="nav-list__item">
                    <NavLink activeClassName="active" to="/users"><ReactSVG src={icons.users}/><span>Users</span></NavLink>
                </li>
            </ul>
        );
};

export default NavigationBar;
