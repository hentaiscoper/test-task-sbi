import React, { Component } from 'react';
import { connect } from "react-redux";
import profile from "../../assets/images/profile.png";
import icons from "../../constants/svgs";
import "./userCard.scss";
import PropTypes from 'prop-types'
import ReactSVG from "react-svg";

class UserCard extends Component {
    render() {
        const { userPending, user: { name, username, address: { city, street }, website }, } = this.props;
        return (
            !userPending && <div className="user-card">
                <div className="user-card__background"/>
                <div className="user-card__main-info">
                    <img className="user-card__icon" src={profile} alt="prof"/>
                    <div className="user-card__name">
                        {name}
                    </div>
                    <div className="user-card__username">
                        {`@${username}`}
                    </div>
                    <div className="user-card__description">
                        <ReactSVG src={icons.place}/>
                        <span>{city}, {street}</span>
                        <ReactSVG className="user-card__website" src={icons.link}/>
                        <span>{website}</span>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.usersReducer.user,
    userPending: state.usersReducer.usersPending,
});

UserCard.propTypes = {
    user: PropTypes.shape({
        address: PropTypes.shape({
            city: PropTypes.string,
            street: PropTypes.string,
        }).isRequired
    }),
    website: PropTypes.string,
    userPending: PropTypes.bool
};



export default connect(mapStateToProps) (UserCard);
