import React from 'react';
import PropTypes from 'prop-types';
import './commentComponent.scss';

const CommentComponent = (props) => {
    const { comment } = props;
    return (
        <div className="comment-container">
            { comment.body }
        </div>
    );
};

CommentComponent.propTypes = {
  comment: PropTypes.shape().isRequired,
};

export default CommentComponent;
