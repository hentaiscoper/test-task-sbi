import React from 'react';
import PropTypes from "prop-types";
import profile from "../../assets/images/profile.png";
import './userComponent.scss'
import { Link } from "react-router-dom";

const UserComponent = (props) => {
    const { user } = props;
    return (
            <Link className="user-container"  to={`/user/${user.id}`}>
                <img className="user-container__icon" src={profile} alt="prof"/>
                <div className="user-container__info">
                    <div className="user-container__name">
                        {user.name}
                    </div>
                    <div className="user-container__username">
                        {`@${user.username}`}
                    </div>
                </div>
            </Link>
    );
};

UserComponent.propTypes = {
    user: PropTypes.shape().isRequired,
};

export default UserComponent;
