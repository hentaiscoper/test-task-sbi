import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk from 'redux-thunk';

import './index.css';
import rootReducer from "./reducers/rootReducer";
import App from './App';

const middleWares = [thunk];

const store = createStore(rootReducer, applyMiddleware(...middleWares));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));