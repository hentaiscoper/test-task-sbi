import {CATCH_USERS_ERROR, GET_USERS_SUCCESS, REQUEST_USERS} from "../actions/userActions";

const usersReducer = (state = { users: [], user: { address: { city: '', street: '' }, website: ' ' } }, action) => {
    switch (action.type) {
        case REQUEST_USERS:
            return {...state, usersPending: true};
        case GET_USERS_SUCCESS:
            return {...state,
                [Array.isArray(action.payload) ? `users` : `user`]: action.payload,
                usersPending: false};
        case CATCH_USERS_ERROR:
            return {...state, error: action.payload, usersPending: false};
        default:
            return state;
    }
};

export default usersReducer;