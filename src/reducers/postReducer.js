import {CATCH_POSTS_ERROR, GET_POSTS_SUCCESS} from "../actions/commentsActions";

const postsReducer = (state = { posts: [] }, action) => {
    switch(action.type) {
        case GET_POSTS_SUCCESS:
            return {...state, posts: action.payload};
        case CATCH_POSTS_ERROR:
            return {...state, error: action.payload};
        default:
            return state;
    }
};

export default postsReducer;