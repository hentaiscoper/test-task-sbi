import {GET_COMMENTS_SUCCESS, CATCH_COMMENTS_ERROR, REQUEST_COMMENTS} from "../actions/postActions";

const commentsReducer = (state = { comments: [], }, action) => {
    switch(action.type) {
        case REQUEST_COMMENTS:
            return {...state, commentsPending: true};
        case GET_COMMENTS_SUCCESS:
            return {...state, comments: action.payload.json, id: action.payload.id, commentsPending: false};
        case CATCH_COMMENTS_ERROR:
            return {...state, error: action.payload, commentsPending: false};
        default:
            return state;
    }
};

export default commentsReducer;