import { combineReducers } from "redux";
import usersReducer from "./userReducer";
import postsReducer from "./postReducer";
import commentsReducer from "./commentsReducer"

const rootReducer = combineReducers({
    usersReducer,
    postsReducer,
    commentsReducer,
});

export default rootReducer;